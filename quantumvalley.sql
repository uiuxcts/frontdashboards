-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-03-2019 a las 17:56:45
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `quantumvalley`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agents`
--

CREATE TABLE `agents` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `agents`
--

INSERT INTO `agents` (`id`, `firstName`, `lastName`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Lázaro', 'Hernández', '2019-03-18 11:31:55', '2019-03-18 11:31:55', NULL),
(2, 'Juan', 'Tortajada', '2019-03-18 11:31:55', '2019-03-18 11:31:55', NULL),
(3, 'Lucía', 'Teran', '2019-03-18 11:31:55', '2019-03-18 11:31:55', NULL),
(4, 'Silvia', 'Aparicio', '2019-03-18 11:31:56', '2019-03-18 11:31:56', NULL),
(5, 'Gerardo', 'Hernandez', '2019-03-18 11:31:56', '2019-03-18 11:31:56', NULL),
(6, 'Luis', 'Dilon', '2019-03-18 11:31:56', '2019-03-18 11:31:56', NULL),
(7, 'Vanesa', 'Gancedos', '2019-03-18 11:31:56', '2019-03-18 11:31:56', NULL),
(8, 'Sin Agente', 'Sin Agente', '2019-03-18 11:31:56', '2019-03-18 11:31:56', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `areas`
--

CREATE TABLE `areas` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idAgent` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `areas`
--

INSERT INTO `areas` (`id`, `name`, `idAgent`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Capacitación Becarios', 1, '2019-03-18 11:31:56', '2019-03-18 11:31:56', NULL),
(2, 'Kernel-CT-Santander', 1, '2019-03-18 11:31:56', '2019-03-18 11:31:56', NULL),
(3, 'Entrenamiento Becarios', 1, '2019-03-18 11:31:56', '2019-03-18 11:31:56', NULL),
(4, 'Intervención Ágil', 2, '2019-03-18 11:31:56', '2019-03-18 11:31:56', NULL),
(5, 'Servicios', 2, '2019-03-18 11:31:56', '2019-03-18 11:31:56', NULL),
(6, 'CT-WEB', 3, '2019-03-18 11:31:56', '2019-03-18 11:31:56', NULL),
(7, 'Envio Masivos', 3, '2019-03-18 11:31:56', '2019-03-18 11:31:56', NULL),
(8, 'Ordenación Académica', 4, '2019-03-18 11:31:56', '2019-03-18 11:31:56', NULL),
(9, 'Campus', 5, '2019-03-18 11:31:56', '2019-03-18 11:31:56', NULL),
(10, 'Tecnologías Educativas', 5, '2019-03-18 11:31:56', '2019-03-18 11:31:56', NULL),
(11, 'Sin Asignar', 8, '2019-03-18 11:31:57', '2019-03-18 11:31:57', NULL);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `areasview`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `areasview` (
`numScholars` bigint(21)
,`agent` varchar(383)
,`id` int(10) unsigned
,`name` varchar(191)
,`idAgent` int(10) unsigned
,`created_at` timestamp
,`updated_at` timestamp
,`deleted_at` timestamp
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `companies`
--

CREATE TABLE `companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `companies`
--

INSERT INTO `companies` (`id`, `name`, `profile`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Ambar', '1', '2019-03-18 11:31:57', '2019-03-18 11:31:57', NULL),
(2, 'Diatomea', '1', '2019-03-18 11:31:57', '2019-03-18 11:31:57', NULL),
(3, 'Netkia', '1', '2019-03-18 11:31:57', '2019-03-18 11:31:57', NULL),
(4, 'Cannon', '1', '2019-03-18 11:31:57', '2019-03-18 11:31:57', NULL),
(5, 'Nordic', '1', '2019-03-18 11:31:57', '2019-03-18 11:31:57', NULL),
(6, 'Uneatlantico', '1', '2019-03-18 11:31:57', '2019-03-18 11:31:57', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documents`
--

CREATE TABLE `documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `idArea` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `documentsview`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `documentsview` (
`id` int(10) unsigned
,`name` varchar(191)
,`file` varchar(191)
,`date` date
,`idArea` int(10) unsigned
,`created_at` timestamp
,`updated_at` timestamp
,`deleted_at` timestamp
,`Area` varchar(191)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logs`
--

CREATE TABLE `logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `modelName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idResource` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `actionDone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(37, '2014_10_12_000000_create_users_table', 1),
(38, '2014_10_12_100000_create_password_resets_table', 1),
(39, '2019_03_04_112900_CreateSocialLoginTable', 1),
(40, '2019_03_05_095725_create_permission_tables', 1),
(41, '2019_03_05_095726_create_agents_table', 1),
(42, '2019_03_05_102601_create_areas_table', 1),
(43, '2019_03_05_102736_create_companies_table', 1),
(44, '2019_03_05_103141_create_scholars_table', 1),
(45, '2019_03_05_103340_create_documents_table', 1),
(46, '2019_03_06_200352_scholars_view', 1),
(47, '2019_03_11_092448_create_logs_table', 1),
(48, '2019_03_11_115345_create_notifications_table', 1),
(49, '2019_03_13_092149_documents_view', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(1, 'App\\User', 2),
(1, 'App\\User', 3),
(1, 'App\\User', 4),
(1, 'App\\User', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'user-list', 'web', '2019-03-18 11:31:53', '2019-03-18 11:31:53', NULL),
(2, 'user-create', 'web', '2019-03-18 11:31:53', '2019-03-18 11:31:53', NULL),
(3, 'user-edit', 'web', '2019-03-18 11:31:53', '2019-03-18 11:31:53', NULL),
(4, 'user-delete', 'web', '2019-03-18 11:31:53', '2019-03-18 11:31:53', NULL),
(5, 'user-show', 'web', '2019-03-18 11:31:53', '2019-03-18 11:31:53', NULL),
(6, 'role-list', 'web', '2019-03-18 11:31:53', '2019-03-18 11:31:53', NULL),
(7, 'role-create', 'web', '2019-03-18 11:31:53', '2019-03-18 11:31:53', NULL),
(8, 'role-edit', 'web', '2019-03-18 11:31:54', '2019-03-18 11:31:54', NULL),
(9, 'role-delete', 'web', '2019-03-18 11:31:54', '2019-03-18 11:31:54', NULL),
(10, 'role-show', 'web', '2019-03-18 11:31:54', '2019-03-18 11:31:54', NULL),
(11, 'permissions-list', 'web', '2019-03-18 11:31:54', '2019-03-18 11:31:54', NULL),
(12, 'permissions-create', 'web', '2019-03-18 11:31:54', '2019-03-18 11:31:54', NULL),
(13, 'permissions-edit', 'web', '2019-03-18 11:31:54', '2019-03-18 11:31:54', NULL),
(14, 'permissions-delete', 'web', '2019-03-18 11:31:54', '2019-03-18 11:31:54', NULL),
(15, 'permissions-show', 'web', '2019-03-18 11:31:54', '2019-03-18 11:31:54', NULL),
(16, 'porcentaje-list', 'web', '2019-03-18 11:31:54', '2019-03-18 11:31:54', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', 'web', '2019-03-18 11:31:54', '2019-03-18 11:31:54', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `scholars`
--

CREATE TABLE `scholars` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `schoolYear` enum('Primero','Segundo','Tercero','Cuarto') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dateStart` date DEFAULT NULL,
  `dateFinish` date DEFAULT NULL,
  `scholarshipPercentage` enum('50%','100%') COLLATE utf8mb4_unicode_ci NOT NULL,
  `idArea` int(10) UNSIGNED NOT NULL,
  `idCompany` int(10) UNSIGNED DEFAULT NULL,
  `nationality` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Interno','Externo') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `scholars`
--

INSERT INTO `scholars` (`id`, `firstName`, `lastName`, `schoolYear`, `dateStart`, `dateFinish`, `scholarshipPercentage`, `idArea`, `idCompany`, `nationality`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Van', 'Ondricka', 'Segundo', '2019-03-18', NULL, '50%', 3, 5, NULL, 'Interno', '2019-03-18 11:31:57', '2019-03-18 11:31:57', NULL),
(2, 'Mateo', 'Okuneva', 'Segundo', '2019-03-18', NULL, '50%', 2, 2, NULL, 'Externo', '2019-03-18 11:31:57', '2019-03-18 11:31:57', NULL),
(3, 'Roscoe', 'Goodwin', 'Tercero', '2019-03-18', NULL, '100%', 7, 6, NULL, 'Externo', '2019-03-18 11:31:57', '2019-03-18 11:31:57', NULL),
(4, 'Carmine', 'Aufderhar', 'Segundo', '2019-03-18', NULL, '100%', 1, 2, NULL, 'Externo', '2019-03-18 11:31:57', '2019-03-18 11:31:57', NULL),
(5, 'Alvena', 'Lowe', 'Tercero', '2019-03-18', NULL, '50%', 4, 3, NULL, 'Interno', '2019-03-18 11:31:57', '2019-03-18 11:31:57', NULL),
(6, 'Dock', 'McLaughlin', 'Primero', '2019-03-18', NULL, '100%', 10, 2, NULL, 'Externo', '2019-03-18 11:31:57', '2019-03-18 11:31:57', NULL),
(7, 'Bryana', 'Gleichner', 'Cuarto', '2019-03-18', NULL, '100%', 7, 1, NULL, 'Externo', '2019-03-18 11:31:57', '2019-03-18 11:31:57', NULL),
(8, 'Kaylee', 'Murphy', 'Cuarto', '2019-03-18', NULL, '50%', 11, 1, NULL, 'Externo', '2019-03-18 11:31:57', '2019-03-18 11:31:57', NULL),
(9, 'Dedrick', 'O\'Reilly', 'Primero', '2019-03-18', NULL, '100%', 9, 4, NULL, 'Interno', '2019-03-18 11:31:57', '2019-03-18 11:31:57', NULL),
(10, 'Carolyne', 'Friesen', 'Segundo', '2019-03-18', NULL, '100%', 4, 3, NULL, 'Externo', '2019-03-18 11:31:57', '2019-03-18 11:31:57', NULL),
(11, 'Arnaldo', 'McGlynn', 'Segundo', '2019-03-18', NULL, '100%', 1, 1, NULL, 'Interno', '2019-03-18 11:31:57', '2019-03-18 11:31:57', NULL),
(12, 'Julie', 'Adams', 'Primero', '2019-03-18', NULL, '100%', 7, 6, NULL, 'Interno', '2019-03-18 11:31:58', '2019-03-18 11:31:58', NULL),
(13, 'Jermain', 'Spinka', 'Segundo', '2019-03-18', NULL, '100%', 3, 2, NULL, 'Interno', '2019-03-18 11:31:58', '2019-03-18 11:31:58', NULL),
(14, 'Jazmyn', 'Funk', 'Primero', '2019-03-18', NULL, '50%', 3, 6, NULL, 'Interno', '2019-03-18 11:31:58', '2019-03-18 11:31:58', NULL),
(15, 'Koby', 'Konopelski', 'Tercero', '2019-03-18', NULL, '50%', 10, 1, NULL, 'Interno', '2019-03-18 11:31:58', '2019-03-18 11:31:58', NULL),
(16, 'David', 'Davis', 'Segundo', '2019-03-18', NULL, '100%', 8, 1, NULL, 'Interno', '2019-03-18 11:31:58', '2019-03-18 11:31:58', NULL),
(17, 'Soledad', 'Morissette', 'Cuarto', '2019-03-18', NULL, '100%', 1, 3, NULL, 'Externo', '2019-03-18 11:31:58', '2019-03-18 11:31:58', NULL),
(18, 'Della', 'Tremblay', 'Cuarto', '2019-03-18', NULL, '50%', 5, 2, NULL, 'Interno', '2019-03-18 11:31:58', '2019-03-18 11:31:58', NULL),
(19, 'Micaela', 'Farrell', 'Cuarto', '2019-03-18', NULL, '50%', 8, 4, NULL, 'Interno', '2019-03-18 11:31:58', '2019-03-18 11:31:58', NULL),
(20, 'Antoinette', 'O\'Kon', 'Tercero', '2019-03-18', NULL, '100%', 4, 1, NULL, 'Interno', '2019-03-18 11:31:58', '2019-03-18 11:31:58', NULL),
(21, 'Kristoffer', 'Miller', 'Tercero', '2019-03-18', NULL, '50%', 7, 6, NULL, 'Interno', '2019-03-18 11:31:58', '2019-03-18 11:31:58', NULL),
(22, 'Lyda', 'Conn', 'Cuarto', '2019-03-18', NULL, '50%', 5, 4, NULL, 'Externo', '2019-03-18 11:31:58', '2019-03-18 11:31:58', NULL),
(23, 'Carmela', 'Mayer', 'Cuarto', '2019-03-18', NULL, '100%', 11, 3, NULL, 'Interno', '2019-03-18 11:31:58', '2019-03-18 11:31:58', NULL),
(24, 'Karina', 'Wolf', 'Tercero', '2019-03-18', NULL, '100%', 4, 2, NULL, 'Interno', '2019-03-18 11:31:58', '2019-03-18 11:31:58', NULL),
(25, 'Jada', 'Tillman', 'Tercero', '2019-03-18', NULL, '50%', 9, 2, NULL, 'Externo', '2019-03-18 11:31:58', '2019-03-18 11:31:58', NULL),
(26, 'Delilah', 'Considine', 'Tercero', '2019-03-18', NULL, '100%', 5, 5, NULL, 'Interno', '2019-03-18 11:31:58', '2019-03-18 11:31:58', NULL),
(27, 'Rachael', 'Legros', 'Tercero', '2019-03-18', NULL, '50%', 11, 5, NULL, 'Interno', '2019-03-18 11:31:58', '2019-03-18 11:31:58', NULL),
(28, 'Aileen', 'Schuppe', 'Primero', '2019-03-18', NULL, '50%', 7, 4, NULL, 'Externo', '2019-03-18 11:31:58', '2019-03-18 11:31:58', NULL),
(29, 'Carmela', 'Murray', 'Tercero', '2019-03-18', NULL, '100%', 8, 1, NULL, 'Externo', '2019-03-18 11:31:58', '2019-03-18 11:31:58', NULL),
(30, 'Kirstin', 'Bernhard', 'Segundo', '2019-03-18', NULL, '100%', 1, 5, NULL, 'Externo', '2019-03-18 11:31:58', '2019-03-18 11:31:58', NULL),
(31, 'Danika', 'Monahan', 'Cuarto', '2019-03-18', NULL, '50%', 8, 2, NULL, 'Externo', '2019-03-18 11:31:58', '2019-03-18 11:31:58', NULL),
(32, 'Valentina', 'Toy', 'Primero', '2019-03-18', NULL, '50%', 11, 1, NULL, 'Interno', '2019-03-18 11:31:58', '2019-03-18 11:31:58', NULL),
(33, 'Titus', 'Ullrich', 'Segundo', '2019-03-18', NULL, '50%', 11, 4, NULL, 'Externo', '2019-03-18 11:31:59', '2019-03-18 11:31:59', NULL),
(34, 'Sammie', 'Bernier', 'Segundo', '2019-03-18', NULL, '50%', 8, 2, NULL, 'Externo', '2019-03-18 11:31:59', '2019-03-18 11:31:59', NULL),
(35, 'Gay', 'McCullough', 'Tercero', '2019-03-18', NULL, '100%', 1, 4, NULL, 'Externo', '2019-03-18 11:31:59', '2019-03-18 11:31:59', NULL),
(36, 'Audie', 'Daniel', 'Tercero', '2019-03-18', NULL, '50%', 2, 3, NULL, 'Externo', '2019-03-18 11:31:59', '2019-03-18 11:31:59', NULL),
(37, 'Amie', 'Reilly', 'Segundo', '2019-03-18', NULL, '50%', 8, 5, NULL, 'Interno', '2019-03-18 11:31:59', '2019-03-18 11:31:59', NULL),
(38, 'Frederick', 'Fahey', 'Primero', '2019-03-18', NULL, '100%', 3, 4, NULL, 'Interno', '2019-03-18 11:31:59', '2019-03-18 11:31:59', NULL),
(39, 'Shanie', 'Tromp', 'Tercero', '2019-03-18', NULL, '50%', 8, 1, NULL, 'Externo', '2019-03-18 11:31:59', '2019-03-18 11:31:59', NULL),
(40, 'Talia', 'Lakin', 'Segundo', '2019-03-18', NULL, '50%', 4, 3, NULL, 'Interno', '2019-03-18 11:31:59', '2019-03-18 11:31:59', NULL),
(41, 'Rosetta', 'Kiehn', 'Tercero', '2019-03-18', NULL, '50%', 10, 4, NULL, 'Externo', '2019-03-18 11:31:59', '2019-03-18 11:31:59', NULL),
(42, 'Belle', 'Leffler', 'Primero', '2019-03-18', NULL, '50%', 1, 1, NULL, 'Externo', '2019-03-18 11:31:59', '2019-03-18 11:31:59', NULL),
(43, 'Lenny', 'Murazik', 'Primero', '2019-03-18', NULL, '50%', 11, 4, NULL, 'Externo', '2019-03-18 11:31:59', '2019-03-18 11:31:59', NULL),
(44, 'Sydnee', 'Lesch', 'Primero', '2019-03-18', NULL, '100%', 1, 6, NULL, 'Externo', '2019-03-18 11:31:59', '2019-03-18 11:31:59', NULL),
(45, 'Darrel', 'Rath', 'Primero', '2019-03-18', NULL, '50%', 3, 1, NULL, 'Externo', '2019-03-18 11:31:59', '2019-03-18 11:31:59', NULL),
(46, 'Shyanne', 'Breitenberg', 'Cuarto', '2019-03-18', NULL, '50%', 10, 4, NULL, 'Externo', '2019-03-18 11:31:59', '2019-03-18 11:31:59', NULL),
(47, 'Gwendolyn', 'White', 'Segundo', '2019-03-18', NULL, '50%', 2, 6, NULL, 'Externo', '2019-03-18 11:31:59', '2019-03-18 11:31:59', NULL),
(48, 'Deon', 'Morissette', 'Segundo', '2019-03-18', NULL, '100%', 10, 1, NULL, 'Interno', '2019-03-18 11:31:59', '2019-03-18 11:31:59', NULL),
(49, 'Alayna', 'Auer', 'Cuarto', '2019-03-18', NULL, '50%', 9, 6, NULL, 'Interno', '2019-03-18 11:31:59', '2019-03-18 11:31:59', NULL),
(50, 'Trevion', 'Stracke', 'Segundo', '2019-03-18', NULL, '50%', 6, 1, NULL, 'Externo', '2019-03-18 11:31:59', '2019-03-18 11:31:59', NULL),
(51, 'Dariana', 'Kerluke', 'Primero', '2019-03-18', NULL, '100%', 9, 2, NULL, 'Externo', '2019-03-18 11:31:59', '2019-03-18 11:31:59', NULL),
(52, 'Korey', 'Mayert', 'Cuarto', '2019-03-18', NULL, '50%', 5, 6, NULL, 'Interno', '2019-03-18 11:31:59', '2019-03-18 11:31:59', NULL),
(53, 'Adelle', 'Effertz', 'Tercero', '2019-03-18', NULL, '100%', 8, 4, NULL, 'Externo', '2019-03-18 11:32:00', '2019-03-18 11:32:00', NULL),
(54, 'Noble', 'Schuster', 'Tercero', '2019-03-18', NULL, '50%', 4, 1, NULL, 'Interno', '2019-03-18 11:32:00', '2019-03-18 11:32:00', NULL),
(55, 'Florine', 'Parker', 'Cuarto', '2019-03-18', NULL, '100%', 3, 5, NULL, 'Interno', '2019-03-18 11:32:00', '2019-03-18 11:32:00', NULL),
(56, 'Owen', 'Rath', 'Tercero', '2019-03-18', NULL, '50%', 9, 2, NULL, 'Interno', '2019-03-18 11:32:00', '2019-03-18 11:32:00', NULL),
(57, 'Yesenia', 'Block', 'Cuarto', '2019-03-18', NULL, '100%', 6, 5, NULL, 'Interno', '2019-03-18 11:32:00', '2019-03-18 11:32:00', NULL),
(58, 'Ismael', 'McLaughlin', 'Primero', '2019-03-18', NULL, '100%', 4, 6, NULL, 'Interno', '2019-03-18 11:32:00', '2019-03-18 11:32:00', NULL),
(59, 'Ivory', 'Ruecker', 'Primero', '2019-03-18', NULL, '50%', 3, 2, NULL, 'Interno', '2019-03-18 11:32:00', '2019-03-18 11:32:00', NULL),
(60, 'Alfreda', 'Eichmann', 'Tercero', '2019-03-18', NULL, '100%', 3, 2, NULL, 'Externo', '2019-03-18 11:32:00', '2019-03-18 11:32:00', NULL),
(61, 'Terrance', 'Weissnat', 'Tercero', '2019-03-18', NULL, '100%', 5, 4, NULL, 'Interno', '2019-03-18 11:32:00', '2019-03-18 11:32:00', NULL),
(62, 'Presley', 'Frami', 'Primero', '2019-03-18', NULL, '50%', 11, 4, NULL, 'Externo', '2019-03-18 11:32:00', '2019-03-18 11:32:00', NULL),
(63, 'Dejon', 'Daniel', 'Tercero', '2019-03-18', NULL, '50%', 8, 5, NULL, 'Interno', '2019-03-18 11:32:00', '2019-03-18 11:32:00', NULL),
(64, 'Evert', 'Hilpert', 'Segundo', '2019-03-18', NULL, '100%', 11, 2, NULL, 'Externo', '2019-03-18 11:32:00', '2019-03-18 11:32:00', NULL),
(65, 'Maryse', 'Huels', 'Primero', '2019-03-18', NULL, '50%', 11, 1, NULL, 'Interno', '2019-03-18 11:32:00', '2019-03-18 11:32:00', NULL),
(66, 'Demario', 'Muller', 'Tercero', '2019-03-18', NULL, '100%', 8, 3, NULL, 'Interno', '2019-03-18 11:32:00', '2019-03-18 11:32:00', NULL),
(67, 'Pansy', 'Koss', 'Cuarto', '2019-03-18', NULL, '100%', 5, 3, NULL, 'Interno', '2019-03-18 11:32:00', '2019-03-18 11:32:00', NULL),
(68, 'Unique', 'Douglas', 'Primero', '2019-03-18', NULL, '50%', 10, 1, NULL, 'Externo', '2019-03-18 11:32:00', '2019-03-18 11:32:00', NULL),
(69, 'Helene', 'Hansen', 'Cuarto', '2019-03-18', NULL, '50%', 4, 6, NULL, 'Interno', '2019-03-18 11:32:00', '2019-03-18 11:32:00', NULL),
(70, 'Ernestine', 'Bernier', 'Segundo', '2019-03-18', NULL, '50%', 6, 4, NULL, 'Interno', '2019-03-18 11:32:00', '2019-03-18 11:32:00', NULL),
(71, 'Mossie', 'Schimmel', 'Tercero', '2019-03-18', NULL, '100%', 9, 4, NULL, 'Interno', '2019-03-18 11:32:00', '2019-03-18 11:32:00', NULL),
(72, 'Darrion', 'Gerhold', 'Segundo', '2019-03-18', NULL, '50%', 9, 4, NULL, 'Interno', '2019-03-18 11:32:00', '2019-03-18 11:32:00', NULL),
(73, 'Melvin', 'Hayes', 'Segundo', '2019-03-18', NULL, '100%', 1, 2, NULL, 'Externo', '2019-03-18 11:32:00', '2019-03-18 11:32:00', NULL),
(74, 'Calista', 'Hyatt', 'Segundo', '2019-03-18', NULL, '50%', 5, 6, NULL, 'Externo', '2019-03-18 11:32:00', '2019-03-18 11:32:00', NULL),
(75, 'Francisca', 'Vandervort', 'Tercero', '2019-03-18', NULL, '100%', 10, 3, NULL, 'Interno', '2019-03-18 11:32:00', '2019-03-18 11:32:00', NULL),
(76, 'Shemar', 'O\'Keefe', 'Tercero', '2019-03-18', NULL, '50%', 10, 6, NULL, 'Externo', '2019-03-18 11:32:01', '2019-03-18 11:32:01', NULL),
(77, 'Destiney', 'Romaguera', 'Primero', '2019-03-18', NULL, '50%', 5, 3, NULL, 'Externo', '2019-03-18 11:32:01', '2019-03-18 11:32:01', NULL),
(78, 'Lennie', 'Green', 'Cuarto', '2019-03-18', NULL, '50%', 6, 5, NULL, 'Externo', '2019-03-18 11:32:01', '2019-03-18 11:32:01', NULL),
(79, 'Louvenia', 'Boyle', 'Cuarto', '2019-03-18', NULL, '50%', 5, 4, NULL, 'Interno', '2019-03-18 11:32:01', '2019-03-18 11:32:01', NULL),
(80, 'Mack', 'Hilpert', 'Cuarto', '2019-03-18', NULL, '100%', 7, 1, NULL, 'Externo', '2019-03-18 11:32:01', '2019-03-18 11:32:01', NULL),
(81, 'Nadia', 'Spencer', 'Tercero', '2019-03-18', NULL, '50%', 7, 3, NULL, 'Interno', '2019-03-18 11:32:01', '2019-03-18 11:32:01', NULL),
(82, 'Hillard', 'Padberg', 'Segundo', '2019-03-18', NULL, '100%', 1, 5, NULL, 'Externo', '2019-03-18 11:32:01', '2019-03-18 11:32:01', NULL),
(83, 'Jayne', 'Reilly', 'Tercero', '2019-03-18', NULL, '50%', 8, 6, NULL, 'Interno', '2019-03-18 11:32:01', '2019-03-18 11:32:01', NULL),
(84, 'Cathy', 'Ruecker', 'Primero', '2019-03-18', NULL, '100%', 8, 3, NULL, 'Interno', '2019-03-18 11:32:01', '2019-03-18 11:32:01', NULL),
(85, 'Clarabelle', 'Orn', 'Primero', '2019-03-18', NULL, '100%', 3, 1, NULL, 'Interno', '2019-03-18 11:32:01', '2019-03-18 11:32:01', NULL),
(86, 'Frederique', 'Hoeger', 'Primero', '2019-03-18', NULL, '100%', 6, 3, NULL, 'Interno', '2019-03-18 11:32:01', '2019-03-18 11:32:01', NULL),
(87, 'Gussie', 'Deckow', 'Tercero', '2019-03-18', NULL, '50%', 8, 2, NULL, 'Interno', '2019-03-18 11:32:01', '2019-03-18 11:32:01', NULL),
(88, 'Elliott', 'Kiehn', 'Tercero', '2019-03-18', NULL, '100%', 6, 1, NULL, 'Interno', '2019-03-18 11:32:01', '2019-03-18 11:32:01', NULL),
(89, 'Angeline', 'Connelly', 'Segundo', '2019-03-18', NULL, '100%', 6, 5, NULL, 'Interno', '2019-03-18 11:32:01', '2019-03-18 11:32:01', NULL),
(90, 'Cassandre', 'Brakus', 'Primero', '2019-03-18', NULL, '100%', 2, 6, NULL, 'Externo', '2019-03-18 11:32:01', '2019-03-18 11:32:01', NULL),
(91, 'Hildegard', 'Dare', 'Primero', '2019-03-18', NULL, '50%', 1, 3, NULL, 'Interno', '2019-03-18 11:32:01', '2019-03-18 11:32:01', NULL),
(92, 'Miller', 'Kutch', 'Tercero', '2019-03-18', NULL, '100%', 11, 2, NULL, 'Interno', '2019-03-18 11:32:01', '2019-03-18 11:32:01', NULL),
(93, 'Marques', 'Stamm', 'Cuarto', '2019-03-18', NULL, '50%', 4, 3, NULL, 'Externo', '2019-03-18 11:32:01', '2019-03-18 11:32:01', NULL),
(94, 'Karlee', 'Pfeffer', 'Tercero', '2019-03-18', NULL, '50%', 10, 3, NULL, 'Interno', '2019-03-18 11:32:01', '2019-03-18 11:32:01', NULL),
(95, 'Sienna', 'Homenick', 'Cuarto', '2019-03-18', NULL, '50%', 4, 4, NULL, 'Externo', '2019-03-18 11:32:01', '2019-03-18 11:32:01', NULL),
(96, 'Ubaldo', 'Mosciski', 'Segundo', '2019-03-18', NULL, '50%', 7, 6, NULL, 'Interno', '2019-03-18 11:32:01', '2019-03-18 11:32:01', NULL),
(97, 'Kayla', 'Mann', 'Tercero', '2019-03-18', NULL, '50%', 11, 4, NULL, 'Interno', '2019-03-18 11:32:01', '2019-03-18 11:32:01', NULL),
(98, 'Myrtle', 'Boehm', 'Cuarto', '2019-03-18', NULL, '50%', 3, 5, NULL, 'Externo', '2019-03-18 11:32:01', '2019-03-18 11:32:01', NULL),
(99, 'Junior', 'Howell', 'Cuarto', '2019-03-18', NULL, '50%', 11, 4, NULL, 'Externo', '2019-03-18 11:32:01', '2019-03-18 11:32:01', NULL),
(100, 'Gino', 'Ruecker', 'Primero', '2019-03-18', NULL, '100%', 6, 5, NULL, 'Interno', '2019-03-18 11:32:01', '2019-03-18 11:32:01', NULL);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `scholarsview`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `scholarsview` (
`id` int(10) unsigned
,`firstName` varchar(191)
,`lastName` varchar(191)
,`schoolYear` enum('Primero','Segundo','Tercero','Cuarto')
,`dateStart` date
,`dateFinish` date
,`scholarshipPercentage` enum('50%','100%')
,`idArea` int(10) unsigned
,`idCompany` int(10) unsigned
,`nationality` varchar(191)
,`status` enum('Interno','Externo')
,`created_at` timestamp
,`updated_at` timestamp
,`deleted_at` timestamp
,`company` varchar(191)
,`area` varchar(191)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `social_logins`
--

CREATE TABLE `social_logins` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `provider` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `social_logins`
--

INSERT INTO `social_logins` (`id`, `user_id`, `provider`, `social_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 5, 'google', '111881952398565608900', '2019-03-18 11:32:27', '2019-03-18 11:32:27', NULL),
(2, 5, 'google', '111881952398565608900', '2019-03-20 15:33:11', '2019-03-20 15:33:11', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`, `token`) VALUES
(1, 'Administrador', 'admin@uneatlantico.es', NULL, '$2y$10$aNN5eADLGeNFl8Xq.u/QZuwHEfTxLxunoTRDQNKmyFYfsgcEGanCS', NULL, '2019-03-18 11:31:52', '2019-03-18 11:31:52', NULL, NULL),
(2, 'Sara Berbil', 'sara.berbil@alumnos.uneatlantico.es', NULL, '', NULL, '2019-03-18 11:31:53', '2019-03-18 11:31:53', NULL, NULL),
(3, 'Abraham Fernandez', 'abraham.fernandez@alumnos.uneatlantico.es', NULL, '', NULL, '2019-03-18 11:31:53', '2019-03-18 11:31:53', NULL, NULL),
(4, 'Pablo Herrero', 'pablo.herrero@alumnos.uneatlantico.es', NULL, '', NULL, '2019-03-18 11:31:53', '2019-03-18 11:31:53', NULL, NULL),
(5, 'Victor', 'victor.martinez@alumnos.uneatlantico.es', NULL, '', 'I5KPg9ss6oPpkuhJjxl98dESVQalQBPYGBhbTCJHwhbmdPMRgOSWAOPo1CcR', '2019-03-18 11:31:53', '2019-03-18 11:31:53', NULL, NULL),
(6, 'Lazaro Hernandez', 'lazaro.hernandez@uneatlantico.es', NULL, '', NULL, '2019-03-18 11:31:53', '2019-03-18 11:31:53', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura para la vista `areasview`
--
DROP TABLE IF EXISTS `areasview`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `areasview`  AS  select count(`sc`.`id`) AS `numScholars`,concat(`ag`.`firstName`,' ',`ag`.`lastName`) AS `agent`,`ar`.`id` AS `id`,`ar`.`name` AS `name`,`ar`.`idAgent` AS `idAgent`,`ar`.`created_at` AS `created_at`,`ar`.`updated_at` AS `updated_at`,`ar`.`deleted_at` AS `deleted_at` from ((`areas` `ar` join `agents` `ag` on((`ar`.`idAgent` = `ag`.`id`))) join `scholars` `sc` on((`ar`.`id` = `sc`.`idArea`))) group by `ar`.`id` order by `ar`.`id` ;

-- --------------------------------------------------------

--
-- Estructura para la vista `documentsview`
--
DROP TABLE IF EXISTS `documentsview`;

CREATE ALGORITHM=UNDEFINED DEFINER=`illuminati`@`localhost` SQL SECURITY DEFINER VIEW `documentsview`  AS  select `d`.`id` AS `id`,`d`.`name` AS `name`,`d`.`file` AS `file`,`d`.`date` AS `date`,`d`.`idArea` AS `idArea`,`d`.`created_at` AS `created_at`,`d`.`updated_at` AS `updated_at`,`d`.`deleted_at` AS `deleted_at`,`ar`.`name` AS `Area` from (`documents` `d` join `areas` `ar` on((`d`.`idArea` = `ar`.`id`))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `scholarsview`
--
DROP TABLE IF EXISTS `scholarsview`;

CREATE ALGORITHM=UNDEFINED DEFINER=`illuminati`@`localhost` SQL SECURITY DEFINER VIEW `scholarsview`  AS  select `sc`.`id` AS `id`,`sc`.`firstName` AS `firstName`,`sc`.`lastName` AS `lastName`,`sc`.`schoolYear` AS `schoolYear`,`sc`.`dateStart` AS `dateStart`,`sc`.`dateFinish` AS `dateFinish`,`sc`.`scholarshipPercentage` AS `scholarshipPercentage`,`sc`.`idArea` AS `idArea`,`sc`.`idCompany` AS `idCompany`,`sc`.`nationality` AS `nationality`,`sc`.`status` AS `status`,`sc`.`created_at` AS `created_at`,`sc`.`updated_at` AS `updated_at`,`sc`.`deleted_at` AS `deleted_at`,`c`.`name` AS `company`,`a`.`name` AS `area` from ((`scholars` `sc` join `companies` `c` on((`sc`.`idCompany` = `c`.`id`))) join `areas` `a` on((`sc`.`idArea` = `a`.`id`))) ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `agents`
--
ALTER TABLE `agents`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `areas_idagent_foreign` (`idAgent`);

--
-- Indices de la tabla `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `documents_idarea_foreign` (`idArea`);

--
-- Indices de la tabla `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indices de la tabla `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indices de la tabla `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `scholars`
--
ALTER TABLE `scholars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `scholars_idarea_foreign` (`idArea`),
  ADD KEY `scholars_idcompany_foreign` (`idCompany`);

--
-- Indices de la tabla `social_logins`
--
ALTER TABLE `social_logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_logins_user_id_index` (`user_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `agents`
--
ALTER TABLE `agents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `areas`
--
ALTER TABLE `areas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `scholars`
--
ALTER TABLE `scholars`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT de la tabla `social_logins`
--
ALTER TABLE `social_logins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `areas`
--
ALTER TABLE `areas`
  ADD CONSTRAINT `areas_idagent_foreign` FOREIGN KEY (`idAgent`) REFERENCES `agents` (`id`);

--
-- Filtros para la tabla `documents`
--
ALTER TABLE `documents`
  ADD CONSTRAINT `documents_idarea_foreign` FOREIGN KEY (`idArea`) REFERENCES `areas` (`id`);

--
-- Filtros para la tabla `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `scholars`
--
ALTER TABLE `scholars`
  ADD CONSTRAINT `scholars_idarea_foreign` FOREIGN KEY (`idArea`) REFERENCES `areas` (`id`),
  ADD CONSTRAINT `scholars_idcompany_foreign` FOREIGN KEY (`idCompany`) REFERENCES `companies` (`id`);

--
-- Filtros para la tabla `social_logins`
--
ALTER TABLE `social_logins`
  ADD CONSTRAINT `social_logins_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
