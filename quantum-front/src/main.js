import Vue from 'vue';
import App from './App.vue';
import router from './router';
import './registerServiceWorker';
import axiosApi from 'axios';
import ElementUI from 'element-ui';
import VueDataTables from 'vue-data-tables';
import VueApexCharts from 'vue-apexcharts'

require('./assets/venus.css');

Vue.use(ElementUI);
Vue.use(VueApexCharts)
Vue.use(VueDataTables)
Vue.component('apexchart', VueApexCharts)

const axios = axiosApi.create({
    //baseURL:'http://localhost/quantumvalley/public/',
    baseURL:'http://172.22.1.115:8081/quantumvalley/public/',
    //baseURL:'http://localhost:3000/',
    //withCredentials: true,
    //mode: 'no-cors',
   // headers:{ 
   // 	'Content-Type': 'application/json'

    //}
   	
});

//Use the window object to make it available globally.

Vue.config.productionTip = false

window.axios = axios;

new Vue({
  router,
  render: function (h) { return h(App) }
}).$mount('#app')
