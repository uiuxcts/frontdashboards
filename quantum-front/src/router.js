import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Becarios from './views/Becarios.vue'
import CreateBecarios from './views/CreateBecarios.vue'
import Graficos from './views/graficos.vue'
import Asistencias from './views/Asistencias.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'index',
      component: Home
    },
    {
      path: '/home',
      name: 'home',
      component: Home
    },
    {
      path: '/becarios',
      name: 'becarios',
      component: Becarios
    },
    {
      path: '/graficos',
      name: 'graficos',
      component: Graficos
    },
    {
      path: '/becarioscrear',
      name: 'create-becarios',
      component: CreateBecarios
    },
    {
      path: '/asistencias',
      name: 'asistencias',
      component: Asistencias
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: function () { 
        return import(/* webpackChunkName: "about" */ './views/About.vue')
      }
    }
  ]
})
